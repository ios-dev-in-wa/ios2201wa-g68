//
//  ViewController.swift
//  G68L3
//
//  Created by Ivan Vasilevich on 1/29/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		let pi = 3.14159
		print("pi", pi, pi, "a")
		var candyCount: Double = 30.99.rounded()
		print("candy Count:", candyCount)
		// + - * / %
		// 15 % 7 == 1
		let multyplier = 2
		candyCount = (candyCount - 1.0) * Double(multyplier)
		// += -= *= /= %=
		candyCount -= 1
		print("candy Count:", candyCount)
		let newCount = candyCount - pi
		
		let candyNumber = Int.random(in: 1...3)
		print("throw candy #\(candyNumber) to Ivan na zadney parte")
		
		foo()
	}
	
	func foo() {
		let pi = 12
		let candyNumber = Int.random(in: 1...3)
		
		let sum1 = sum(a: pi, b: candyNumber)
		let sum2 = sum(a: Int.random(in: 1...3), b: Int.random(in:
			1...3))
		let sum3 = sum1 + sum2
		print("summSumm = \(sum3)")
		bar()
	}
	
	func sum(a: Int, b: Int) -> Int {
		let result = a + b
		//		< > <= >= == != !young
		let resultIsLow = result < 100 //false true
		if resultIsLow  {
			print("resultIsLow")
			return result
		}
//		let c = 99
		print(a, "+", b, "=", result)
		return result
	}
	
	func bar() {
		let maxMoney = 4000
		let money = Int.random(in: 1...maxMoney)
		let age = Int.random(in: 1...3)
		
		let young = age != 3
		let rich = money > maxMoney / 2
		
		// and && ||
		// 1 1 && ||
		// 0 1 ||
		// 1 0 ||
		// 0 0
		if young && !rich {
			print("ok")
		}
		else {
			print("WASTED")
		}
		rich || young ? print("ok") : print("WASTED")
		print(rich || young ? "ok" : "WASTED")
		
	}
	
	func ifElseSwitch() {
		let candyNumber = Int.random(in: 1...4)
		
		if candyNumber == 1 {
			print("//Dasha")
		}
		else if candyNumber == 1 {
			print("//Dasha")
		}
		else if candyNumber == 2 {
			print("//Ruslan")
		}
		else {
			print("//Anyone")
		}
		
		switch candyNumber {
		case 1:
			print("//Dasha")
			print("//Dasha")
		case 2:
			print("//Ruslan")
		default:
			print("//Anyone")
		}
	}
	
	


}

