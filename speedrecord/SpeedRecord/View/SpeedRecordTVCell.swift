//
//  SpeedRecordTVCell.swift
//  SpeedRecord
//
//  Created by Ivan Vasilevich on 2/19/19.
//  Copyright © 2019 Ivan Besarab. All rights reserved.
//

import UIKit

class SpeedRecordTVCell: UITableViewCell {
	
	@IBOutlet weak var speedLabel: UILabel!
	@IBOutlet weak var timeLabel: UILabel!
	
    
}
