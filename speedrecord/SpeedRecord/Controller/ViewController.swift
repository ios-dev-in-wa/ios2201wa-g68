//
//  ViewController.swift
//  SpeedRecord
//
//  Created by Ivan Besarab on 2/13/19.
//  Copyright © 2019 Ivan Besarab. All rights reserved.
//

import UIKit
import Foundation
import CoreLocation
import Charts

let metersPerSecondToKmPerHourMultiplier = 3.6

class ViewController: UIViewController  {
	
	@IBOutlet var chartView: LineChartView!
	@IBOutlet weak var shadowViewWidthConstraint: NSLayoutConstraint!
	@IBOutlet weak var tableView: UITableView!
	
	var speedRecords = [CLLocation]()
    
    let manager = CLLocationManager()
    var currentSpeed = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        manager.requestWhenInUseAuthorization()
        
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.startUpdatingLocation()
		
		let celNib = UINib(nibName: "SpeedRecordTVCell", bundle: nil)
		tableView.register(celNib, forCellReuseIdentifier: "SpeedRecordTVCell")
		tableView.dataSource = self
		
		
	}
	
	override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
		return .portrait
	}
	
	func setDataCount(_ count: Int, range: UInt32) {
		
//		let arra
		
		var values = [ChartDataEntry]()
		
		for i in 0..<speedRecords.count {
			let val = speedRecords[i].speed * metersPerSecondToKmPerHourMultiplier
			values.append(ChartDataEntry(x: Double(i), y: val))
		}
		
//		values.reverse()
		
		let set1 = LineChartDataSet(values: values, label: "DataSet 1")
		set1.drawIconsEnabled = false
		
		set1.lineDashLengths = [5, 2.5]
		set1.highlightLineDashLengths = [5, 2.5]
		set1.setColor(.black)
		set1.setCircleColor(.black)
		set1.lineWidth = 1
		set1.circleRadius = 3
		set1.drawCircleHoleEnabled = false
		set1.valueFont = .systemFont(ofSize: 9)
		set1.formLineDashLengths = [5, 2.5]
		set1.formLineWidth = 1
		set1.formSize = 15
		
		let gradientColors = [ChartColorTemplates.colorFromString("#00ff0000").cgColor,
							  ChartColorTemplates.colorFromString("#ffff0000").cgColor]
		let gradient = CGGradient(colorsSpace: nil, colors: gradientColors as CFArray, locations: nil)!
		
		set1.fillAlpha = 1
		set1.fill = Fill(linearGradient: gradient, angle: 90) //.linearGradient(gradient, angle: 90)
		set1.drawFilledEnabled = true
		
		let data = LineChartData(dataSet: set1)
		
		chartView.data = data
	}
	
	
}

extension ViewController: CLLocationManagerDelegate {
	// MARK: - CLLocationManagerDelegate
	func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
		guard let location = manager.location else {
			return
		}
		
		speedRecords.insert(location, at: 0)
		shadowViewWidthConstraint.constant = view.frame.width *  CGFloat(speedRecords.count) / 10
		tableView.reloadData()
		setDataCount(10, range: 44)
	}
	
	func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
		print("Error: \(error.localizedDescription)")
	}
	
}

extension ViewController: UITableViewDataSource {
	// MARK: - UITableViewDataSource
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return speedRecords.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "SpeedRecordTVCell", for: indexPath) as! SpeedRecordTVCell
		let objectToDisplay = speedRecords[indexPath.row]
		cell.speedLabel.text = (objectToDisplay.speed * metersPerSecondToKmPerHourMultiplier).roundToDecimal(2).description + " km/h"
		
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "HH:mm:ss.SSS"

		dateFormatter.locale = Locale(identifier: "en_US")
		let subtitle = (dateFormatter.string(from: objectToDisplay.timestamp))
		
		cell.timeLabel.text = subtitle
		
		return cell
	}
}

extension ViewController: UITableViewDelegate {
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		print(indexPath)
	}
}
