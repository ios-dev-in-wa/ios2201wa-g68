//
//  Double+Round.swift
//  SpeedRecord
//
//  Created by Ivan Vasilevich on 2/14/19.
//  Copyright © 2019 Ivan Besarab. All rights reserved.
//

import Foundation

extension Double {
	func roundToDecimal(_ fractionDigits: Int) -> Double {
		let multiplier = pow(10, Double(fractionDigits))
		return Darwin.round(self * multiplier) / multiplier
	}
}
