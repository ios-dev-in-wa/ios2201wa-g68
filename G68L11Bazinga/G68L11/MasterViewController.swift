//
//  MasterViewController.swift
//  G68L11
//
//  Created by Ivan Vasilevich on 2/26/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit
import Parse

class MasterViewController: UITableViewController {

	var detailViewController: DetailViewController? = nil
	var objects = [PFObject]()
	var characters = [User]()
	var currentPage = 1


	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		if PFUser.current() == nil {
			let loginBtn = UIBarButtonItem(title: "Login",
										   style: .plain,
										   target: self,
										   action: #selector(showLoginVC))
			
			navigationItem.leftBarButtonItem = loginBtn
		}
		
		loadMoreData()
		
		if let split = splitViewController {
		    let controllers = split.viewControllers
		    detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
		}
	}
	
	func loadMoreData() {
		NetworkService().fetchUsers(forPage: currentPage) { [weak self] users in
			guard let data = users else { return }
			self?.characters.append(contentsOf: data)
			self?.reloadData()
			self?.currentPage += 1
		}
	}

	override func viewWillAppear(_ animated: Bool) {
		clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
		super.viewWillAppear(animated)
		reloadData()
	}
	
	private func reloadData() {
		let query = PFQuery.init(className: "Car")
		query.findObjectsInBackground { (optionalObjects, error) in
			if let realObjects = optionalObjects {
				self.objects = realObjects
				self.tableView.reloadData()
			}
		}
	}


	// MARK: - Segues

	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "showDetail" {
		    if let indexPath = tableView.indexPathForSelectedRow {
		        let object = objects[indexPath.row] as! NSDate
		        let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
		        controller.detailItem = object
		        controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
		        controller.navigationItem.leftItemsSupplementBackButton = true
		    }
		}
	}

	// MARK: - Table View

	override func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}

	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return characters.count
	}
	
	override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
		if indexPath.row == characters.count - 3 {
			loadMoreData()
		}
	}

	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

		let object = characters[indexPath.row]
		cell.textLabel!.text = "\(object.name.first) \(object.name.last) "//["name"] as? String
		return cell
	}

	override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
		// Return false if you do not want the specified item to be editable.
		return true
	}

	override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
		if editingStyle == .delete {
		    objects.remove(at: indexPath.row)
		    tableView.deleteRows(at: [indexPath], with: .fade)
		} else if editingStyle == .insert {
		    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
		}
	}

	@objc func showLoginVC() {
		let loginVC = PFLogInViewController()
		loginVC.delegate = self
		present(loginVC,
				animated: true,
				completion: nil)
	}

}

extension MasterViewController: PFLogInViewControllerDelegate {
	func log(_ logInController: PFLogInViewController, didLogIn user: PFUser) {
		logInController.dismiss(animated: true) {
			self.navigationItem.leftBarButtonItem = nil
		}
	}
}

