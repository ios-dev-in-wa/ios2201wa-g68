//
//  MasterViewController.swift
//  G68L16
//
//  Created by Ivan Vasilevich on 3/21/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit
import CoreData

class MasterViewController: UITableViewController, NSFetchedResultsControllerDelegate {
	
//	enabled_CoreData_preference

	
	var coreDataEnabled: Bool {
		return UserDefaults.standard.bool(forKey: "enabled_CoreData_preference")
	}
	
	var objects = [Event]()
	
	var _fetchedResultsController: NSFetchedResultsController<Event>? = nil

	var detailViewController: DetailViewController? = nil
	var managedObjectContext: NSManagedObjectContext? = nil


	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		navigationItem.leftBarButtonItem = editButtonItem

		let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(insertNewObject(_:)))
		navigationItem.rightBarButtonItem = addButton
		if let split = splitViewController {
		    let controllers = split.viewControllers
		    detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
		}
		
		fetchData()
		
	}
	
	func fetchData() {
		if !coreDataEnabled {
			return
		}
		
		let fetchRequest: NSFetchRequest<Event> = Event.fetchRequest()
		
		// Set the batch size to a suitable number.
		fetchRequest.fetchBatchSize = 2000
		
		// Edit the sort key as appropriate.
		let sortDescriptor = NSSortDescriptor(key: "timestamp", ascending: false)
		
		fetchRequest.sortDescriptors = [sortDescriptor]
		
		let result = try? managedObjectContext!.fetch(fetchRequest)
		
		let firstRes = result!.first
		
		objects = result ?? objects
//		print(result!.first?.fotos)
	}

	override func viewWillAppear(_ animated: Bool) {
		clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
		super.viewWillAppear(animated)
	}

	@objc
	func insertNewObject(_ sender: Any) {
		let context = self.fetchedResultsController.managedObjectContext
		let newEvent = Event(context: context)
		     
		// If appropriate, configure the new managed object.
		newEvent.timestamp = Date()

		objects.append(newEvent)
		tableView.reloadData()
		// Save the context.
		if !coreDataEnabled {
			return
		}
		do {
		    try context.save()
		} catch {
		    // Replace this implementation with code to handle the error appropriately.
		    // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
		    let nserror = error as NSError
		    fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
		}
	}

	// MARK: - Segues

	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "showDetail" {
		    if let indexPath = tableView.indexPathForSelectedRow {
		    let object = objects[indexPath.row]
		        let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
		        controller.detailItem = object
		        controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
		        controller.navigationItem.leftItemsSupplementBackButton = true
		    }
		}
	}

	// MARK: - Table View

	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		
		return objects.count
	}

	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
		let event = objects[indexPath.row]
		configureCell(cell, withEvent: event)
		return cell
	}

	override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
		// Return false if you do not want the specified item to be editable.
		return true
	}

	override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
		if editingStyle == .delete {
		    let context = fetchedResultsController.managedObjectContext
		    context.delete(fetchedResultsController.object(at: indexPath))
		        
		    do {
		        try context.save()
		    } catch {
		        // Replace this implementation with code to handle the error appropriately.
		        // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
		        let nserror = error as NSError
		        fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
		    }
		}
	}

	func configureCell(_ cell: UITableViewCell, withEvent event: Event) {
		cell.textLabel!.text = event.timestamp!.description
	}

	// MARK: - Fetched results controller

	var fetchedResultsController: NSFetchedResultsController<Event> {
	    if _fetchedResultsController != nil {
	        return _fetchedResultsController!
	    }
	    
	    let fetchRequest: NSFetchRequest<Event> = Event.fetchRequest()
	    
	    // Set the batch size to a suitable number.
	    fetchRequest.fetchBatchSize = 2000
	    
	    // Edit the sort key as appropriate.
	    let sortDescriptor = NSSortDescriptor(key: "timestamp", ascending: false)
	    
	    fetchRequest.sortDescriptors = [sortDescriptor]
	    
	    // Edit the section name key path and cache name if appropriate.
	    // nil for section name key path means "no sections".
	    let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.managedObjectContext!, sectionNameKeyPath: nil, cacheName: "Master")
	    aFetchedResultsController.delegate = self
	    _fetchedResultsController = aFetchedResultsController
	    
	    do {
	        try _fetchedResultsController!.performFetch()
	    } catch {
	         // Replace this implementation with code to handle the error appropriately.
	         // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
	         let nserror = error as NSError
	         fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
	    }
	    
	    return _fetchedResultsController!
	}    
	
	

//	func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
//	    tableView.beginUpdates()
//	}
//
//	func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
//	    switch type {
//	        case .insert:
//	            tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
//	        case .delete:
//	            tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
//	        default:
//	            return
//	    }
//	}
//
//	func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
//	    switch type {
//	        case .insert:
//	            tableView.insertRows(at: [newIndexPath!], with: .fade)
//	        case .delete:
//	            tableView.deleteRows(at: [indexPath!], with: .fade)
//	        case .update:
//	            configureCell(tableView.cellForRow(at: indexPath!)!, withEvent: anObject as! Event)
//	        case .move:
//	            configureCell(tableView.cellForRow(at: indexPath!)!, withEvent: anObject as! Event)
//	            tableView.moveRow(at: indexPath!, to: newIndexPath!)
//	    }
//	}
//
//	func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
//	    tableView.endUpdates()
//	}



}

