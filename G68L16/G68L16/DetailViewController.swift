//
//  DetailViewController.swift
//  G68L16
//
//  Created by Ivan Vasilevich on 3/21/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit
import MapKit

class DetailViewController: UIViewController {

	@IBOutlet weak var detailDescriptionLabel: UILabel!

	let manager = CLLocationManager.init()

	func configureView() {
		// Update the user interface for the detail item.
		if let detail = detailItem {
		    if let label = detailDescriptionLabel {
		        label.text = detail.timestamp!.description
		    }
		}
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		configureView()
		
//		manager.delegate = self
		
		
		
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.denied {
			
		}
		manager.requestWhenInUseAuthorization()
	}

	var detailItem: Event? {
		didSet {
		    // Update the view.
		    configureView()
		}
	}


}

