//
//  ViewController.swift
//  G68L4
//
//  Created by Ivan Vasilevich on 1/31/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		
		let name = "Iv\nan"
		
		print("Created\nby \(name) on " + "1/31/19")
		
		var longString = """
//	  ViewController.swift
" fdsfsdfsd"
//  G68L4
//
//  Created by  Ivan Vasilevich  on 1/31/19.
//  Copyright © 2019 , RockSoft. All rights reserved.
"""
//		printSeparator(title: "String Example")
		brintSeparatorWithTitle("String Example")
		print(longString)
		longString = longString.replacingOccurrences(of: "  ", with: " ")
		brintSeparatorWithTitle()
		print(longString)
		longString = longString.replacingOccurrences(of: " , ", with:  ",")
		brintSeparatorWithTitle()
//		print(longString)
		let prefix = "//\t"
		if longString.hasPrefix(prefix) {
			print("long str starts with \"\(prefix)\"")
		}
		else {
			print("long str dont starts with \"\(prefix)\"")
		}
		
		arrayEample()
		
	}
	
	func brintSeparatorWithTitle(_ title: String = "default value") {
		print("----------------------\(title)----------------------")
	}
	
	func arrayEample() {
		let student0 = "Oleg"
		let student1 = "Sasha"
		let student2 = "Ivan"
		let student3 = "Dasha"
		
		let studentX = "MishaX"
		
		var students = [student0, student1, student2, student3]
		students.append("Ruslan")
		students.insert("Misha", at: 2)
		
		brintSeparatorWithTitle("arrayEample")
//		for i in 0..<students.count {
//			brintSeparatorWithTitle(students[i])
//		}
		greeteng(names: students)
		
		brintSeparatorWithTitle()
		let optionalIndex: Int? = students.firstIndex(of: studentX)
		// optional binding
		if let realIndex = optionalIndex {
			print("studentX index: \(realIndex)")
		}
		else {
			print("studentX not found in arrey")
		}
		
		// force unwrap
//		print("studentX index: \(optionalIndex!)")
		
		// default value
		print("studentX index: \(optionalIndex ?? students.count)")
		
//		let optionalNumber = Int("1")
		
		dictionaryExample()
	}
	
	func greeteng(names: [String]) {
		for i in 0..<names.count {
			print("\(i)) Hello!", names[i] )
		}
	}
	
	func dictionaryExample() {
		let phoneBook = ["Police" 		: "102",
						 "Ambulance" 	: "103",
						 "FireFighters" : "101"]
		
		print(phoneBook["Holice"])
		phoneBook.count
		for something in phoneBook {
			print(something.value)
		}
		
		dzArray()
	}
	
	func dzArray() {
		var array = [Int].init()
		
		for _ in 0..<20 {
			array.append(Int.random(in: 0...10))
		}
		
		print(array)
	}

	
	

}

class Car {
	
}

