//
//  YellowVC.swift
//  G68L7
//
//  Created by Ivan Vasilevich on 2/12/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class YellowVC: UIViewController {

	@IBOutlet weak var centerLabel: UILabel!
	
	override func viewDidDisappear(_ animated: Bool) {
		super.viewDidDisappear(animated)
		
		view.backgroundColor = UIColor(red: CGFloat(arc4random_uniform(256))/255,
									   green: CGFloat(arc4random_uniform(256))/255,
									   blue: CGFloat(arc4random_uniform(256))/255,
									   alpha: 1)
	}

}
