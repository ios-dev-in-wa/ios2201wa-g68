//
//  ImageDisplayVC.swift
//  G68L7
//
//  Created by Ivan Vasilevich on 2/12/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class ImageDisplayVC: UIViewController {
	
	@IBOutlet weak var imageBox: UIImageView!
	
	var imageName = ""

	override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
		let image = UIImage(named: imageName)
		imageBox.image = image
    }
    

}
