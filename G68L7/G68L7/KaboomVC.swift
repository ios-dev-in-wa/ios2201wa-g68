//
//  KaboomVC.swift
//  G68L7
//
//  Created by Ivan Vasilevich on 2/12/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class KaboomVC: UIViewController {
	
	@IBOutlet weak var previewImageBox: UIImageView!
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		let image = UIImage.init(named: "flash_bang")!
		view.backgroundColor = UIColor(patternImage: image)
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//		if let imageDisplayVC = segue.destination as? ImageDisplayVC {
//			if let senderImageName = sender as? String {
//				imageDisplayVC.imageName = senderImageName
//			}
//		}
		
		guard let imageDisplayVC = segue.destination as? ImageDisplayVC else  {
			return
		}
		
		guard let senderImageName = sender as? String  else {
			return
		}
		
		imageDisplayVC.imageName = senderImageName
		
	}
	
	override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
		if identifier == "showSnow" {
			return false
		}
		return true
	}

	@IBAction func pictureSelected(_ sender: UIButton) {
		print(sender.tag)
		let image = UIImage(named: "\(sender.tag)")
		previewImageBox.image = image
		performSegue(withIdentifier: "showSnow", sender: sender.tag.description)
	}
	
	@IBAction func showMapVC(_ sender: UISegmentedControl) {
		if sender.selectedSegmentIndex == 1  {
			let mapVC = storyboard?.instantiateViewController(withIdentifier: "mapVC")
			mapVC?.title = "map 0006"
			mapVC?.hidesBottomBarWhenPushed = true
			navigationController?.pushViewController(mapVC!, animated: true)
		}
	}
}
