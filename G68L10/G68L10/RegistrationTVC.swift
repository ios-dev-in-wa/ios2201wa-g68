//
//  RegistrationTVC.swift
//  G68L10
//
//  Created by Ivan Vasilevich on 2/26/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class RegistrationTVC: UITableViewController {

  
	@IBOutlet var textFields: [UITextField]!
	
	@IBAction func fillUserData(_ sender: UIBarButtonItem) {
		UIView.transition(with: view,
						  duration: 2,
						  options: [.transitionCrossDissolve],
						  animations: {
			self.textFields.forEach {
				$0.text = Int.random(in: 0..<999999999).description
			}
		},
						  completion: nil)
		
	}
}
