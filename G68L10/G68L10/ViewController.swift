//
//  ViewController.swift
//  G68L10
//
//  Created by Ivan Vasilevich on 2/21/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	@IBAction func tapRecognized(_ sender: UITapGestureRecognizer) {
		let pos = sender.location(in: view)
		
//		UIView.animate(withDuration: 0.3, animations: {
//			self.faceView.center = pos
//		}) { [weak self] (finished) in
//			if finished {
//				self?.faceView.setNeedsDisplay()
//				self?.faceView.happyLevel = 40
//				print("animation finished")
//			}
//		}
		
		UIView.animate(withDuration: 10, delay: 1, options: [.allowUserInteraction, .curveEaseIn, .autoreverse], animations: {
			self.faceView.center = pos
		}) { [weak self] (finished) in
			if finished {
				self?.faceView.setNeedsDisplay()
				self?.faceView.happyLevel = 40
				print("animation finished")
			}
		}
		
	}
	@IBOutlet weak var faceView: FaceView!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		faceView.happyLevel = 21
		faceView.changeHappyFace(withHappLevel: 35, andColor: .red)
		
		var sortCount = 0
		
		var array = [4, 6, 9, 1, 14]
		array.sort { [weak self]  (numA, numB) -> Bool in
			let result = numA < numB
			sortCount += 1
			self?.foo()
			return result
		}
		
		
		
		
		
		
	}
	
	
	func foo() {
		print("hello")
	}


}

