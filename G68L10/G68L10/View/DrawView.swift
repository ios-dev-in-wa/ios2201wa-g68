//
//  DrawView.swift
//  G68L10
//
//  Created by Ivan Vasilevich on 2/21/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class DrawView: UIView {
	
	func setup() {
		backgroundColor = .red
	}

	override func awakeFromNib() {
		super.awakeFromNib()
		setup()
	}
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		setup()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
//		fatalError("init(coder:) has not been implemented")
	}
	
	override func draw(_ rect: CGRect) {
		let path = UIBezierPath()
		path.move(to: CGPoint(x: 20, y: 40))
		path.addLine(to: CGPoint(x: 60, y: 0.5 * bounds.height))
		path.addLine(to: CGPoint(x: 60, y: 40))
		path.close()
		
		path.lineWidth = 2
		
		UIColor.lightGray.setStroke()
		UIColor.blue.setFill()
		
		path.fill()
		path.stroke()
	}
	
}
