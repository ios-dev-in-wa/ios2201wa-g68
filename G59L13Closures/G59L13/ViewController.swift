//
//  ViewController.swift
//  G59L13
//
//  Created by Ivan Vasilevich on 1/18/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit
import SDWebImage

class ViewController: UIViewController {

	@IBOutlet weak var imageBox: UIImageView!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		NotificationCenter.default.addObserver(self,
											   selector: #selector(foo(notification:)),
											   name: imageDownloadedNotification,
											   object: nil)
		
		NotificationCenter.default.addObserver(forName: imageDownloadedNotification,
											   object: self,
											   queue: OperationQueue.main) { [unowned self] (notification) in
			self.bar()
		}
		
	}
	
	override func viewDidDisappear(_ animated: Bool) {
		super.viewDidDisappear(animated)
		
		NotificationCenter.default.removeObserver(self)
	}

	@IBAction func loadImage(_ sender: UIBarButtonItem) {
		let link = "http://speedhunters-wp-production.s3.amazonaws.com/wp-content/uploads/2018/09/05214851/2018-Speedhunters_Niki-Lauda-Vintage_Trevor-Ryan-004_.jpg"

//		var image: UIImage?
//		kBgQ.async {
//			image = ImageDownloader4000().downloadImageFromLink(link: link)
//			kMainQueue.async {
//				self.imageBox.image = image
//			}
//		}
		
//		ImageDownloader4000().downloadImageFromLink(link: link) { (img) in
//			self.imageBox.image = img
//		}
		
//		ImageDownloader4000().downloadImageFromLink(link: link)
		
//		ImageDownloader4000().downloadImageFromLink(link: link) { (img) in
//			self.imageBox.image = img
//			print(link)
//		}
		
		
//		present(UIViewController(), animated: false, completion: nil)
		
//		ImageDownloader4000().callClusureAfterDelay(delay: 5) {
//			print("http://fuckingclosuresyntax.com/")
//		}
//		ImageDownloader4000.init().downloadImageFromLink(link: link)
		
		imageBox.sd_setImage(with: URL.init(string: link)!, completed: nil)

	}
	
	@IBAction func getForecast(_ sender: UIBarButtonItem) {
	
		ImageDownloader4000().sendForecastRequest()
	}
	
	@objc func foo(notification: Notification) {
		print("fooofofofofofooo", (notification.userInfo?["pic"] as? UIImage) ?? UIImage())
		let img = (notification.userInfo?["pic"] as? UIImage) ?? UIImage()
		imageBox.image = img
	}
	
	func bar() {
		print("barbar ar!")
	}
	
	func showAlert(message: String) {
//		var alert = UIAlertController(title: "Valentin", message: "How to display alert with input fields", preferredStyle: .alert)
//		let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (act) in
//			print("cancel pressed")
//		}
//		alert.addAction(cancelAction)
//
//		let okAction = UIAlertAction(title: "Ok", style: .default) { (act) in
//			print("OK pressed")
//			print(alert.textFields?.first?.text ?? "no password")
//		}
//		alert.addAction(okAction)
//
//		alert.addTextField { (textField) in
//			textField.placeholder = "sekretnuy parol6"
//			textField.isSecureTextEntry = true
//		}
//
		let alert = UIStoryboard.init(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "qwertyuiop")
		present(alert, animated: true, completion: nil)
	}
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		showAlert(message: "")
	}
}

