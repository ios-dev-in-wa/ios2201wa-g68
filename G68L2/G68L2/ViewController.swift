//
//  ViewController.swift
//  G68L2
//
//  Created by Ivan Vasilevich on 1/24/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
//		print("AYAYAYAY")
//		sayHello()
		let abc = 12
//		let sumOfSquares2 = squareOfNumber() + squareOfNumber()
		squareOfNumber(inputNumber: 5)
		squareOfNumber(inputNumber: 10)
		let squareOfNumberAbc = squareOfNumber(inputNumber: abc, printCount: 1)
		print(squareOfNumberAbc)
		drawBox()
		var d = 100
		for i in 0..<5 {
			d = d - i
			print("hello #\(i)")
		}
		print("hello #\(d)")
	}
	
	func sayHello() {
		let a = 5
//		a = 1
		var b = 6
		
		print("sayHello")
		print(a)
		print(b)
		b = 7
//		print(abc)
	}
	
	func squareOfNumber() -> Int {
		let number = 2
		let result = squareOfNumber(inputNumber: number, printCount: 1)
		return result
	}
	
	func squareOfNumber(inputNumber: Int) {
		_ = squareOfNumber(inputNumber: inputNumber, printCount: 1)
		for _ in 0..<25 {
			
		}
	}
	
	func squareOfNumber(inputNumber: Int, printCount: Int) -> Int {
		let number = inputNumber
		let result = number * number
		for _ in 0..<printCount {
			print("Square of number \(number) equals \(result)")
		}
		return result
	}
	
	func drawBox() {
		let box = UIView()
		box.frame.size.width = 64
		box.frame.size.height = 64
		box.frame.origin.x = 100
		box.frame.origin.y = 10
		box.backgroundColor = UIColor(red: CGFloat(arc4random_uniform(256))/255,
									  green: CGFloat(arc4random_uniform(256))/255,
									  blue: CGFloat(arc4random_uniform(256))/255,
									  alpha: 1)
		view.addSubview(box)
	}


}

