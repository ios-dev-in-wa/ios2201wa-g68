//
//  ViewController.swift
//  G68L12
//
//  Created by Ivan Vasilevich on 2/28/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit
import AVFoundation

struct MyStruct {
	var a: String = "a"
	var b: Int = 2
	var c: Double
}

class MyClass {
	var a: String = "a"
	var b: Int = 2
}

class ViewController: UIViewController {

	@IBOutlet weak var nameTextField: UITextField!
	@IBOutlet weak var surnameTextField: UITextField!
	@IBOutlet weak var aboutTextView: UITextView!
	
	var player: AVAudioPlayer?
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		foo()
		
		nameTextField.delegate = self
	}
	
	func foo() {
		let classInstance1 = MyClass()
		var structInstance1 = MyStruct(a: "a", b: 2, c: 33)
		
		let structInstance2 = structInstance1
		let classInstance2 = classInstance1
		
		structInstance1.a = "dsjdklajsdlkdjla"
		classInstance1.a = "djdi23hd87282"
		
		print(structInstance1)
		print(structInstance2)
		
		print(classInstance1.a)
		print(classInstance2.a)
	}
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		
		view.endEditing(true)
		
		guard let fileURL = Bundle.main.url(forResource: "pew-pew-lei", withExtension: "caf") else {
			return
		}
		do {
			player = try AVAudioPlayer(contentsOf: fileURL)
		}
		catch {
			print(error.localizedDescription)
		}
		
		
		
		player?.prepareToPlay()
		player?.play()
	}

}

extension ViewController: UITextFieldDelegate {
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		if textField == nameTextField {
			surnameTextField.becomeFirstResponder()
		}
		if textField == surnameTextField {
			textField.resignFirstResponder()
		}
		return false
	}
}

