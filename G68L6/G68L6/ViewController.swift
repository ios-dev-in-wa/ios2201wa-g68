//
//  ViewController.swift
//  G68L6
//
//  Created by Ivan Vasilevich on 2/7/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	
	@IBOutlet weak var imageView: UIImageView!
	
	override func awakeFromNib() {
		super.awakeFromNib()
		
		imageView?.image = nil
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		let filePath = "/Users/ivanvasilevich/Desktop/arr.plist"
		saveToFile(["1", "2", "ALiababa"], filePath: filePath)
		print(readFromFile(filePath))
		
		addRunCount()
		
		colorPlay()
		
		imagePlay()
		
		let a: Int! = 5
		let b: Int? = 6
		let c = a + (b ?? 0)
		print("c =", c)
		
	}
	
	func saveToFile(_ arrayToSave: [String], filePath: String) {
		let convertedArr = arrayToSave as NSArray
		convertedArr.write(toFile: filePath, atomically: false)
	}
	
	func readFromFile(_ filePath: String) -> NSArray {
		let arr = NSArray.init(contentsOfFile: filePath)
		return arr!
	}

	func addRunCount() {
		let kRunCount = "e3d7g239dt623txdg78d23fx"
		let runCount = UserDefaults.standard.integer(forKey: kRunCount) + 1
		print("runCount: \(runCount)")
		UserDefaults.standard.set(runCount, forKey: kRunCount)
	}

	func colorPlay() {
		let color = UIColor(rgb: 0xE52C0B)
//		let c = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
		let flowerColor = UIColor.init(red: 255/255.0,
									   green: 33/255.0,
									   blue: 65/255.0,
									   alpha: 1)
		view.backgroundColor = flowerColor
		
	}
	
	func imagePlay() {
		let image = UIImage(named: "GregHowell-Porsche-964-2017-jordanbutters-speedhunters-5961-1200x801")
		imageView.image = image
	}
}

