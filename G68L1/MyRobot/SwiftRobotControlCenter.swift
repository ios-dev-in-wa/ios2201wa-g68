//
//  SwiftRobotControlCenter.swift
//  MyRobot
//
//  Created by Ivan Vasilevich on 10/4/14.
//  Copyright (c) 2014 Ivan Besarab. All rights reserved.
//

import UIKit

class SwiftRobotControlCenter: RobotControlCenter {
	
	override func viewDidLoad() {
		levelName = "L4H"
		
		super.viewDidLoad()
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		firstLine()
		while frontIsClear {
			move()
		}
		turnRight()
		secondLine()
	}
	
	func turnLeft() {
		for _ in 0..<3 {
			turnRight()
		}
	}
	
	func firstLine() {
		cross()
		turnRight()
	}
	
	func secondLine() {
		cross()
		put()
	}
	
	func cross() {
		while frontIsClear {
			put()
			turnRight()
			
			if frontIsClear {
				move()
				turnLeft()
				move()
			}
		}
	}
}
