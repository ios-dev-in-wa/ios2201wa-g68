//
//  DataViewController.swift
//  G68L15P2
//
//  Created by Ivan Vasilevich on 3/14/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class DataViewController: UIViewController {

	@IBOutlet weak var dataLabel: UILabel!
	var dataObject: String = ""

	@IBOutlet weak var imageView: UIImageView!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		dataLabel!.text = dataObject
		imageView.image = UIImage(named: dataObject)
		
	}


}

