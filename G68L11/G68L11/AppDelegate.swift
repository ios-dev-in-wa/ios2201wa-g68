//
//  AppDelegate.swift
//  G68L11
//
//  Created by Ivan Vasilevich on 2/26/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit
import Parse

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UISplitViewControllerDelegate {

	var window: UIWindow?


	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		// Override point for customization after application launch.
		let splitViewController = window!.rootViewController as! UISplitViewController
		let navigationController = splitViewController.viewControllers[splitViewController.viewControllers.count-1] as! UINavigationController
		navigationController.topViewController!.navigationItem.leftBarButtonItem = splitViewController.displayModeButtonItem
		splitViewController.delegate = self
		
		initParse()
		
		return true
	}
	
	private func initParse() {
		let config = ParseClientConfiguration { (conf) in
			conf.applicationId = "m66HpZYmjiQoOe4I46UmVXw4OJYTgp9QtffuVU7E"
			conf.clientKey = "KOvkQlg08v277gPqv1c1mVCR3I3mefsDfwDf4Y4T"
			
			conf.server = "https://parseapi.back4app.com/"
			//            conf.isLocalDatastoreEnabled = true
		}
		Parse.initialize(with: config)
		
	}

	// MARK: - Split view

	func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController:UIViewController, onto primaryViewController:UIViewController) -> Bool {
	    guard let secondaryAsNavController = secondaryViewController as? UINavigationController else { return false }
	    guard let topAsDetailController = secondaryAsNavController.topViewController as? DetailViewController else { return false }
	    if topAsDetailController.detailItem == nil {
	        // Return true to indicate that we have handled the collapse by doing nothing; the secondary controller will be discarded.
	        return true
	    }
	    return false
	}

}

