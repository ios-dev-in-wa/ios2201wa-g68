//
//  AddItemTVC.swift
//  G68L11
//
//  Created by Ivan Vasilevich on 2/26/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit
import Parse

class AddItemTVC: UITableViewController {

	@IBOutlet weak var itemNameTextField: UITextField!
	@IBOutlet weak var accidentSwitch: UISwitch!
	
	@IBAction func cancellPressed(_ sender: UIBarButtonItem) {
		dismiss(animated: true, completion: nil)
	}
	
	@IBAction func savePressed(_ sender: UIBarButtonItem) {
		//save item
		let newCar = PFObject.init(className: "Car")
		newCar["name"] = itemNameTextField.text
		newCar["wasInAccident"] = accidentSwitch.isOn
		newCar["firstOwner"] = PFUser.current()
		newCar.saveInBackground { (success, error) in
			
			self.cancellPressed(sender)
			guard let user = PFUser.current(), success else {
				return
			}
			user.relation(forKey: "cars").add(newCar)
			user.saveInBackground()
		}
		
		
	}
	
}
