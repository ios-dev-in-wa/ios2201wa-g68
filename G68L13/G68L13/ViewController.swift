//
//  ViewController.swift
//  G68L13
//
//  Created by Ivan Vasilevich on 3/5/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	@IBOutlet weak var webView: UIWebView!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		let url = Bundle.main.url(forResource: "video", withExtension: "mov")!//URL(string: "https://www.youtube.com/watch?v=DHB42n9n6pA")!
//		webView.loadRequest(URLRequest(url: url))
		
	}
	
	func showAlert() {
		let alert = UIAlertController(title: "title",
									  message: "message",
									  preferredStyle: .actionSheet)
		
		alert.addTextField { (tf) in
			tf.placeholder = "AZAZAZAS"
		}
		
		alert.addAction(UIAlertAction(title: "Cancel",
									  style: .cancel,
									  handler: { (_) in
										print("Cancel pressed", alert.textFields?.first?.text ?? "no text")
		}))
		
		if UIDevice.current.userInterfaceIdiom == .pad {
			alert.modalPresentationStyle = .popover
			
			alert.popoverPresentationController?.sourceView = self.view
			alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
			alert.popoverPresentationController?.permittedArrowDirections = []
		}
		
		
		
		present(alert,
				animated: true,
				completion: nil)
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		showAlert()
	}


}

