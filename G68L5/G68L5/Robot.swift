//
//  Robot.swift
//  G68L5
//
//  Created by Ivan Vasilevich on 2/5/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit


class Robot: NSObject {
	
	var health = 5
	private var candyCount = 0
	
	var width = 5
	var height = 2
	
	var candy: Int {
		return candyCount
	}
	
	var perimeter: Int {
		return width * 2 + height * 2
	}
	
	override var description: String {
		return "Robot: health = (\(health)) +\n\(super.description)"
	}
	//	var enemy: Robot
	
	func put() {
		if candyCount > 0 {
			addCandy(-1)
		}
	}
	
	func pick() {
		if candyCount < Int.max {
			addCandy(1)
		}
	}
	
	private func addCandy(_ count: Int) {
		candyCount += count
	}
	
}
