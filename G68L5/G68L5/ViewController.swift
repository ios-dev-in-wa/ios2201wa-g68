//
//  ViewController.swift
//  G68L5
//
//  Created by Ivan Vasilevich on 2/5/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit



class ViewController: UIViewController {

	@IBOutlet weak var krokodilCountLabel: UILabel!
	var krokodilCount = 0
	
	let robot1 = Robot.init()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
	
		print("robot1.candy = \(robot1.candy)")
		robot1.health = 3
		robot1.pick()
		robot1.pick()
		robot1.pick()
		print("robot1.candy = \(robot1.candy)")
		
		let robot2 = Robot.init()
//		robot2.candyCount = 400
		
		let a = 5
//		let aString = a.description
		print(robot1.description, "\n", robot2)
	}

	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		robot1.pick()
		print("robot1.candy: \(robot1.candy)")
	}
	
	@IBAction func buttonPressed() {
		print("MAMA YA ZDALAL")
		view.backgroundColor = UIColor(red: CGFloat(arc4random_uniform(256))/255,
									   green: CGFloat(arc4random_uniform(256))/255,
									   blue: CGFloat(arc4random_uniform(256))/255,
									   alpha: 1)
		addAndDisplayOneKrokodil()
	}
	
	private func addAndDisplayOneKrokodil() {
		robot1.pick()
		krokodilCountLabel.textColor = .green
		krokodilCountLabel.text = "KROKODILOV V HATE: \(robot1.candy)"
	}

}

