//
//  ViewController.swift
//  G68L15
//
//  Created by Ivan Vasilevich on 3/14/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	@IBOutlet weak var scrollView: UIScrollView!
	@IBOutlet weak var pageControll: UIPageControl!

	@IBAction func pageChanged(_ sender: UIPageControl) {
		scrollView.setContentOffset(CGPoint(x: scrollView.frame.width * CGFloat(sender.currentPage), y: 0), animated: true)
	}
	
}

extension ViewController: UIScrollViewDelegate {
	
	func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
		pageControll.currentPage = Int(scrollView.contentOffset.x / scrollView.frame.width)
	}
}

